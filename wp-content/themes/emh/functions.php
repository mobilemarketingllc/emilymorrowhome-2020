<?php
// Load parent theme
add_action('wp_enqueue_scripts', 'emh_enqueue_styles');
function emh_enqueue_styles() {
    wp_enqueue_style('parent-style', get_template_directory_uri() . '/style.css');
}

// Unhook parent theme actions
function unhook_parent_actions() {
    // We want the secondary navigation to appear above the header
    remove_action('storefront_header', 'storefront_secondary_navigation', 30);
    // We want search to be a dropdown in the secondary navigation
    remove_action('storefront_header', 'storefront_product_search', 40);
    // Don't show cart in main navigation
    remove_action('storefront_header', 'storefront_header_cart', 60);
}
add_action('init', 'unhook_parent_actions');

// Add emh body class so we can create CSS rules that override standard Storefront rules
add_filter('body_class', 'emh_body_class');
function emh_body_class($classes) {
    $classes[] = 'emh';
    return $classes;
}

// Write our stylesheet
add_action('wp_head', 'emh_print_styles', 8);
function emh_print_styles() { ?>
<link href="<?php bloginfo('stylesheet_directory'); ?>/assets/public/<?php the_css_with_version(); ?>" rel="stylesheet"
    type="text/css">
<?php }

// Move secondary navigation to sit above the header
add_action('storefront_before_header', 'storefront_secondary_navigation', 30);

// Move search to sit above the header
add_action('storefront_before_header', 'storefront_product_search', 40);

// Customize secondary navigation
function storefront_secondary_navigation() {
    if (has_nav_menu('secondary')) { ?>
<nav class="secondary-navigation" role="navigation" aria-label="<?php esc_html_e('Secondary Navigation', 'storefront'); ?>">
    <ul class="secondary-nav-content message-content">
        <li><a href="/contact-us">Contact Us</a></li>
        <li><a href="/retailers/">Locate a Retailer</a></li>
    </ul>
    <ul class="secondary-nav-content search-content">
        <li><a href="#" id="search-link">Search</a></li>
    </ul>
    <?php
                wp_nav_menu(
                    array(
                        'theme_location'	=> 'secondary',
                        'fallback_cb'		=> '',
                  )
              );
            ?>
</nav><!-- #site-navigation -->
<?php
    }
}

// Customize logo
function storefront_site_title_or_logo($echo = true) {
    $args = array(
        'posts_per_page' => 1,
        'post_type' => 'attachment',
        'name' => "main-logo"
  );
    $get_attachment = new WP_Query($args);
    $logo_attachment = $get_attachment->posts[0];
    if ($logo_attachment) { 
        $img_src = wp_get_attachment_image_url($logo_attachment->ID, 'medium');
        $img_srcset = wp_get_attachment_image_srcset($logo_attachment->ID, 'medium');
        $html = sprintf('<a href="%1$s" class="site-logo-link" rel="home" itemprop="url"><img src="%2$s" srcset="%3$s" sizes="%4$s" /></a>',
            esc_url(home_url('/')),
            esc_url($img_src),
            esc_attr($img_srcset),
            ""
      );
    } else {
        $tag = is_home() ? 'h1' : 'div';

        $html = '<' . esc_attr($tag) . ' class="beta site-title"><a href="' . esc_url(home_url('/')) . '" rel="home">' . esc_html(get_bloginfo('name')) . '</a></' . esc_attr($tag) .'>';

        if ('' !== get_bloginfo('description')) {
            $html .= '<p class="site-description">' . esc_html(get_bloginfo('description', 'display')) . '</p>';
        }
    }

    if (!$echo) {
        return $html;
    }

    echo $html;
}

// Remove product page breadcrumbs
add_filter("woocommerce_get_breadcrumb", "__return_false");

// Add a docready function to product pages
add_action('woocommerce_after_single_product', 'emh_after_single_product');
function emh_after_single_product() {
    add_action('wp_footer', 'emh_footer_product_docready', 10);
}

function emh_footer_product_docready() { ?>
<script type="text/javascript">
    jQuery(window).load(function() {
        product_docready(jQuery)
    });
</script>
<?php }

// Override the word "newness" with "newest"
function woocommerce_catalog_ordering() {
    global $wp_query;

    if (1 === (int) $wp_query->found_posts || !woocommerce_products_will_display() || $wp_query->is_search()) {
        return;
    }

    $orderby                 = isset($_GET['orderby']) ? wc_clean($_GET['orderby']) : apply_filters('woocommerce_default_catalog_orderby', get_option('woocommerce_default_catalog_orderby'));
    $show_default_orderby    = 'menu_order' === apply_filters('woocommerce_default_catalog_orderby', get_option('woocommerce_default_catalog_orderby'));
    $catalog_orderby_options = apply_filters('woocommerce_catalog_orderby', array(
        'menu_order' => __('Default sorting', 'woocommerce'),
        'popularity' => __('Sort by popularity', 'woocommerce'),
        'rating'     => __('Sort by average rating', 'woocommerce'),
        'date'       => __('Sort by newest', 'woocommerce'),
        'price'      => __('Sort by price: low to high', 'woocommerce'),
        'price-desc' => __('Sort by price: high to low', 'woocommerce'),
   ));

    if (! $show_default_orderby) {
        unset($catalog_orderby_options['menu_order']);
    }

    if ('no' === get_option('woocommerce_enable_review_rating')) {
        unset($catalog_orderby_options['rating']);
    }

    wc_get_template('loop/orderby.php', array('catalog_orderby_options' => $catalog_orderby_options, 'orderby' => $orderby, 'show_default_orderby' => $show_default_orderby));
}

// Override category/collection page rendering of thumbnails
function woocommerce_template_loop_product_thumbnail() {
    echo '<span class="woocommerce-loop-product-image-container">' . woocommerce_get_product_thumbnail() . '</span>';
}

// Customize order details to indicate return policy
add_action('woocommerce_order_details_after_order_table', 'add_return_policy');
function add_return_policy() { ?>
<h2>Return policy</h2>
<p>All sales are final, but you may request cancellation within the first 48 hours by <a href="/contact-us">contacting
        customer service</a>.</p>
<?php }

// Customize credit section of footer
function storefront_credit() {
    ?>
<p class="site-info">
    <?php echo esc_html(apply_filters('storefront_copyright_text', $content = '&copy; ' . date('Y') . ' ' . get_bloginfo('name'))); ?>
    <br />All rights reserved.
</p><!-- .site-info -->
<?php
}

// Customize footer scripts
add_action('wp_footer', 'emh_footer_scripts', 10);
function emh_footer_scripts() { ?>
<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/assets/public/<?php the_footer_js_with_version(); ?>"></script>
<?php }

// Asset pipeline versioning
function the_css_with_version() {
	the_asset_with_version('styles.min.css');
}

function the_header_js_with_version() {
	the_asset_with_version('header.min.js');
}

function the_footer_js_with_version() {
	the_asset_with_version('footer.min.js');
}

function the_asset_with_version($target) {
	$scripts = file_get_contents(STYLESHEETPATH . '/assets/public/rev-manifest.json');
	$scripts = json_decode($scripts);
	$scripts = get_object_vars($scripts);
	echo $scripts[$target];
}

// Admin: Add min/max quantity fields as well as UOM fields
function emh_woocommerce_product_options_general_product_data() {
    // Minimum quantity
    woocommerce_wp_text_input(array(
        'id' => '_minimum_quantity',
        'label' => 'Minimum Quantity',
        'description' => 'Minimum quantity that must be ordered.',
        'desc_tip' => true,
        'placeholder' => 'no minimum',
        'type' => 'number'
  ));
    
    // Maximum quantity
    woocommerce_wp_text_input(array(
        'id' => '_maximum_quantity',
        'label' => 'Maximum Quantity',
        'description' => 'Maximum quantity that cannot be exceeded.',
        'desc_tip' => true,
        'placeholder' => 'no maximum',
        'type' => 'number'
  ));

    // UOM (singular)
    woocommerce_wp_text_input(array(
        'id' => '_uom_singular',
        'label' => 'UOM (singular)',
        'description' => 'Unit of measure (singular), e.g. "box"',
        'desc_tip' => true,
        'type' => 'text'
  ));

    // UOM (plural)
    woocommerce_wp_text_input(array(
        'id' => '_uom_plural',
        'label' => 'UOM (plural)',
        'description' => 'Unit of measure (plural), e.g. "boxes"',
        'desc_tip' => true,
        'type' => 'text'
  ));
}
add_action('woocommerce_product_options_general_product_data', 'emh_woocommerce_product_options_general_product_data');

function emh_woocommerce_process_product_meta($post_id) {
    // Need this function in order to save entered values
    if (!empty($_POST['_minimum_quantity'])) {
        update_post_meta($post_id, '_minimum_quantity', esc_attr($_POST['_minimum_quantity']));
    }
   // if (!empty($_POST['_maximum_quantity'])) {
        update_post_meta($post_id, '_maximum_quantity', esc_attr($_POST['_maximum_quantity']));
   // }
    if (!empty($_POST['_uom_singular'])) {
        update_post_meta($post_id, '_uom_singular', esc_attr($_POST['_uom_singular']));
    }
    if (!empty($_POST['_uom_plural'])) {
        update_post_meta($post_id, '_uom_plural', esc_attr($_POST['_uom_plural']));
    }
    if (!empty($_POST['_quantity_notes'])) {
        update_post_meta($post_id, '_quantity_notes', esc_attr($_POST['_quantity_notes']));
    }
}
add_action('woocommerce_process_product_meta', 'emh_woocommerce_process_product_meta');

// Product: Enforce min/max quantities
function emh_woocommerce_quantity_input_args($args, $product) {
    // Get min/max from product meta
    $min = null;
    $max = null;
    $uom_singular = null;
    $uom_plural = null;
    $id = get_the_ID();
    if ($product !== null) {
        $id = $product->get_id();
    }
    if ($id !== null) {
        $min = get_post_meta($id, '_minimum_quantity', true);
        $max = get_post_meta($id, '_maximum_quantity', true);
        $uom_singular = get_post_meta($id, '_uom_singular', true);
        $uom_plural = get_post_meta($id, '_uom_plural', true);
    }

    $args['uom_singular'] = $uom_singular;
    $args['uom_plural'] = $uom_plural;

    // Enforce UI limits
    $min = intval($min);
    $max = intval($max);
    if (is_singular('product') && $min !== null && $min > 0) {
        $args['input_value'] = $min;
    }

    if ($min !== null && $min > 0) {
        $args['min_value'] = $min;
    }

    if ($max !== null && $max > 0) {
        $args['max_value'] = $max;
    }
    return $args;
}
add_filter('woocommerce_quantity_input_args', 'emh_woocommerce_quantity_input_args', 1, 2);

// Show 24 products per page on category view
add_filter('loop_shop_per_page', create_function('$cols', 'return 24;'), 20);

// Remove 'Add to Cart' button on category view
add_action('woocommerce_after_shop_loop_item', 'remove_add_to_cart_buttons', 1);
function remove_add_to_cart_buttons() {
    if (is_product_category() || is_shop() || is_tax()) {
        remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart');
    }
}

// Set template loop product title back to an h3 the way it used to be before WC 3.0
function woocommerce_template_loop_product_title() {
    echo '<h3>' . get_the_title() . '</h3>';
}

// Set up taxonomies
function emh_register_taxonomies() {
    register_taxonomy(
        'lifestyles',
        'product',
        array(
            'labels' => array(
                'name' => __('Lifestyles'),
                'singular_name' => __('Lifestyle'),
                'all_items' => __('All Lifestyles'),
                'edit_item' => __('Edit Lifestyle'),
                'view_item' => __('View Lifestyle'),
                'update_item' => __('Update Lifestyle'),
                'add_new_item' => __('Add New Lifestyle'),
                'new_item_name' => __('New Lifestyle'),
                'search_items' => __('Search Lifestyles'),
                'popular_items' => __('Popular Lifestyles'),
                'separate_items_with_commas' => __('Separate lifestyles with commas'),
                'add_or_remove_items' => __('Add or remove lifestyles'),
                'choose_from_most_used' => __('Choose from the most used lifestyles'),
                'not_found' => __('No lifestyles found.')
          ),
            'rewrite' => array(
                'slug' => 'lifestyles'
          )
      )
  );
}
add_action('init', 'emh_register_taxonomies');

// Better featured product titles
function emh_product_title($product) {
    $product_type = get_post_type($product);

    // Variable products
    if ($product_type == 'product_variation') {
        // Get parent product
        $product_parent_id = wp_get_post_parent_id($product);
        $product_parent = get_post($product_parent_id);

        // Get product category 
        $product_category = get_the_terms($product_parent, 'product_cat')[0]->slug;
        if ($product_category == 'furniture') {
            // Furniture: concatenate parent title with Furniture Style value from variant
            $title = $product_parent->post_title;

            // Get furniture type
            $furniture_type = get_post_meta($product->ID, 'attribute_pa_furniture-type', true);
            if (!empty($furniture_type)) {
                $title .= ' - ' . $furniture_type;
            }
            
            return $title;
        }
    }

    return get_the_title($product);
}

// Change title separator to pipe
add_filter('document_title_separator', 'emh_document_title_separator');
function emh_document_title_separator($sep) {
    $sep = "|";
    return $sep;
}

// Add meta tags
add_action('wp_head', 'emh_add_meta_tags', 2);
function emh_add_meta_tags() {
    if (is_single() || is_page()) {
        global $post;

        $meta_desc = get_post_meta($post->ID, 'meta_description', true);
        if (!$meta_desc) {
            $meta_desc = get_the_excerpt($post);
        }
        if ($meta_desc) {
            echo '<meta name="description" content="' . htmlentities($meta_desc) . '" />' . "\n";
        }
    } else if (is_tax()) {
        $meta_desc = get_term_meta(get_queried_object_id(), 'meta_description', true);
        if ($meta_desc) {
            echo '<meta name="description" content="' . htmlentities($meta_desc) . '" />' . "\n";
        }
    }
}

// Custom home title - don't want the "| Emily Morrow Home" suffix, so we can't use the optimized page title technique below
add_filter('pre_get_document_title', 'emh_pre_get_document_title');
function emh_pre_get_document_title() {
    if (is_home() || is_front_page()) {
        return 'Emily Morrow Home | Flooring | Furniture | Home Décor';
    }
}

// Get optimized title for title tag
add_filter('single_post_title', 'emh_single_post_title');
function emh_single_post_title($post_title) {
    global $post;
    $optimized_title = get_post_meta($post->ID, 'optimized_title', true);
    if ($optimized_title) {
        $post_title = $optimized_title;
    }
    return $post_title;
}

add_filter('single_term_title', 'emh_single_term_title');
function emh_single_term_title($term_title) {
    $optimized_title = get_term_meta(get_queried_object_id(), 'optimized_title', true);
    if ($optimized_title) {
        $term_title = $optimized_title;
    }
    return $term_title;
}

// Prevent optimized title from overwriting <h1> title on tax pages
function woocommerce_page_title( $echo = true ) {
    if ( is_search() ) {
        $page_title = sprintf( __( 'Search results: &ldquo;%s&rdquo;', 'woocommerce' ), get_search_query() );

        if ( get_query_var( 'paged' ) ) {
            $page_title .= sprintf( __( '&nbsp;&ndash; Page %s', 'woocommerce' ), get_query_var( 'paged' ) );
        }
    } elseif ( is_tax() ) {
        //$page_title = single_term_title( "", false );
        $term = get_queried_object();
        $page_title = $term->name;
    } else {
        $shop_page_id = wc_get_page_id( 'shop' );
        $page_title   = get_the_title( $shop_page_id );
    }

    $page_title = apply_filters( 'woocommerce_page_title', $page_title );

    if ( $echo ) {
        echo $page_title;
    } else {
        return $page_title;
    }
}

// Better featured product descriptions
function emh_product_description($product) {
    $product_type = get_post_type($product);

    // Variable products
    if ($product_type == 'product_variation') {
        // Get parent product
        $product_parent_id = wp_get_post_parent_id($product);
        $product_parent = get_post($product_parent_id);

        return $product_parent->post_excerpt;
    }

    return $product->post_excerpt;
}


$role_emily = array ('administrator','dealer-1');
$user_emily = wp_get_current_user();
$array_unique = (array_intersect( $role_emily, $user_emily->roles));



// Samples
add_action('woocommerce_after_add_to_cart_button', 'show_samples', 990);
function show_samples() {
    // Product samples, if applicable
    $id = get_the_ID();
	$sample_box_id = get_post_meta($id, 'sample_box', true);
    $sample_square_id = get_post_meta($id, 'sample_square', true);
    $molding_id = get_post_meta($id, 'molding', true);
    $user = wp_get_current_user();
       global $role_emily;
       global $array_unique;
       global $user_emily;
  

    if( is_user_logged_in() && array_intersect( $role_emily, $user_emily->roles)){       

            if ($molding_id) {
                echo '<div class="sample-box">';
                echo '<p>Looking for molding?</p>';
                echo '<ul>';
                $molding = get_post($molding_id);
                echo '<li><a href="' . get_the_permalink($molding_id) . '">' . $molding->post_title . '</a></li>';
                echo '</ul>';
                echo '</div>';
            }
    }
	if ($sample_box_id || $sample_square_id) {
        echo '<div class="sample-box">';
		echo '<p>Looking for samples?</p>';
		echo '<ul>';
		if ($sample_box_id) {
            $sample_box = get_post($sample_box_id);
            if( is_user_logged_in() && array_intersect( $role_emily, $user_emily->roles)){
            echo '<li><a href="' . get_the_permalink($sample_box_id) . '">' . $sample_box->post_title . '</a></li>';
            }
        }
        if ($sample_square_id) {
            $sample_square = get_post($sample_square_id);
            echo '<li><a href="' . get_the_permalink($sample_square_id) . '">' . $sample_square->post_title . '</a></li>';
        }
        echo '</ul>';
        echo '</div>';
    }
 
}

// Blog sidebar widget
add_action('widgets_init', 'blog_widgets_init');
function blog_widgets_init() {
    register_sidebar(array(
        'name' => __('Blog Sidebar', 'woocommerce'),
        'id' => 'emh-blog-sidebar',
    ));
}

/* 
 * Add customer email to Cancelled Order recipient list
 */
 function wc_cancelled_order_add_customer_email( $recipient, $order ){
    return $recipient . ',' . $order->billing_email;
}
add_filter( 'woocommerce_email_recipient_cancelled_order', 'wc_cancelled_order_add_customer_email', 10, 2 );

function my_logged_in_redirect() {
    $ifyes = get_field('is_portal_page');
   // if($ifyes  == 1 && !is_user_logged_in()) 
  //  {
    // wp_redirect('my-account/' );
    // exit;
   // }
}
//add_action( 'template_redirect', 'my_logged_in_redirect' );

function my_own_body_classes($classes) {

    if ( get_post_type( get_the_ID() ) == 'ordersample' ) {
        $classes[] = 'product-template-default single single-product woocommerce woocommerce-page';
    }
    // Go for other posts types here


    return $classes;
}
add_filter('body_class', 'my_own_body_classes');

// shortcode to show username 
function new_username() {
      global $current_user;
      get_currentuserinfo();
      $full_name_acc = $current_user->user_firstname . " " . $current_user->user_lastname;
      if($full_name_acc == "")
      {
          return $username_view = $current_user->user_login;
      }
      else
      {
          return $username_view = $full_name_acc;
      }     
}
add_shortcode('username_code', 'new_username');
 
// shortcode for display order sample 
function new_ordersample( $atts ) {
    if($atts){
        foreach ( $atts as $key => $value ){
            if($key=='cat'){
                $cats = $value;
            }
        } 
    }
    $html_code = '<div class="ordersamples col-3"><ul class="products">';
        $loop = new WP_Query(
            array(
                    'post_type' => 'ordersample',
                    'posts_per_page' => -1,
                    'orderby'=>'name',
                    'order'=>'ASC',
                    'tax_query' => array(
                        array(
                          'taxonomy' => 'product_cat',
                          'field' => 'slug',
                          'terms' => $cats,
                          'include_children' => false
                        )
                    )
                )
        );
            if ( $loop->have_posts() ) :
                while ( $loop->have_posts() ) : $loop->the_post();  
                    $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full'); 
                    $html_code .= '<li class=""><a href="'.get_permalink().'"><span>';
                    $html_code .= '<img src="'.esc_url($featured_img_url).'" width="300">';             
                    $html_code .= '</span><h3>'. get_the_title().'</h3></a></li>';
                endwhile;
            endif;	 
    $html_code .= '</ul></div>';

    return $html_code;
}
add_shortcode('order_sample_code', 'new_ordersample');

// shortcode for display products on flooring 
add_shortcode('portal_flooring_code', 'new_portal_flooring');
function new_portal_flooring( $atts ) {
    foreach ( $atts as $key => $value ){
        if($key=='cat'){
            $cats = $value;
        }
    } 
    $args = array(
        'post_type' => 'product',
        'posts_per_page' => -1,
        'product_cat' => $cats,
        'include_children' => false,
        'orderby'=>'name',
        'order'=>'ASC'
    );
    $loop = new WP_Query( $args );
        $html_cat = '<div class="ordersamples col-3"><ul class="products">';
    //get_the_post_thumbnail($loop->post->ID, 'shop_catalog')product_category category="flooring" columns="3"
        while ( $loop->have_posts() ) : $loop->the_post(); global $product;  
            $featured_pro_img_url = get_the_post_thumbnail_url(get_the_ID(),'full'); 
            $html_cat .= '<li class=""><a href="'.get_permalink( $loop->post->ID ).'">';
            $html_cat .= '<img src="'.esc_url($featured_pro_img_url).'" width="300">';   
        
            $user_chk = wp_get_current_user();
            $role_chk = ( array ) $user_chk->roles;
            if($role_chk[0]=='dealer-1'){      
                $html_cat .= '<h3>'.get_the_title().'</h3>';
               // $html_cat .= '<div class="price">'.$product->get_price_html().'</div>';
            }
            else{
                $html_cat .= '<h3>'.get_the_title().'</h3> ';
            }                    
            $html_cat .= '</a>         
            </li>';
        endwhile; 
        wp_reset_query(); 
        $html_cat .= '</ul>';
    return $html_cat;
}

//Ajax Function for Dropdown change in single material

add_action( 'wp_ajax_nopriv_show_market_material', 'show_market_material' );
add_action( 'wp_ajax_show_market_material', 'show_market_material' );

function show_market_material() {

	global $wpdb;
    $mat_postid = $_POST['material_id']; 
    $material = get_post($mat_postid);

    $result = '';

    while( have_rows('material_gallery',$mat_postid) ): the_row(); 

        $image = get_sub_field('upload_image', $mat_postid);
        $content = get_sub_field('image_title', $mat_postid);       

        $result.='<li class=""><img src="'.$image.'" alt="'.$content.'" width="300" />
        <?php echo $content; ?>
<a href="'.$image.'" download>
    <span class="mat_title">'.$content.'</span>
    <img src="/wp-content/uploads/2018/12/download.png" width="24" />
</a>
</li>';

endwhile;

echo $result;

wp_die();

}

/**
* Update the po box order meta with field value
*/
add_action( 'woocommerce_checkout_update_order_meta', 'my_custom_checkout_field_update_order_meta' );

function my_custom_checkout_field_update_order_meta( $order_id ) {
if ( ! empty( $_POST['pobox'] ) ) {
update_post_meta( $order_id, 'PO Box Number', sanitize_text_field( $_POST['pobox'] ) );
}
}

/**
* Display po box value on the order edit page
*/
add_action( 'woocommerce_admin_order_data_after_billing_address', 'my_custom_checkout_field_display_admin_order_meta',
10, 1 );

function my_custom_checkout_field_display_admin_order_meta($order){
echo '<p><strong>'.__('PO Box').':</strong> ' . get_post_meta( $order->id, 'PO Box Number', true ) . '</p>';
}


/**
* Add a po box number (in an order) to the emails
*/
add_filter( 'woocommerce_email_order_meta_fields', 'custom_woocommerce_email_order_meta_fields', 10, 3 );

function custom_woocommerce_email_order_meta_fields( $fields, $sent_to_admin, $order ) {

// this order meta for po box number
$pobox = get_post_meta( $order->id, 'PO Box Number', true );

// we won't display anything if it is not a gift
if( !empty( $pobox ) ){

echo '<br>
<h2>PO BOX Details</h2>
<ul>
    <li><strong>PO Box:</strong> ' . $pobox . '</li>
</ul>';
}
}

// The email to send can be changed by using a different filter in place of 'woocommerce_email_recipient_new_order'
// The payment method can be changed by using a different payment method id in place of 'invoice'
add_filter( 'woocommerce_email_recipient_new_order', 'wc_new_order_billmelater_recipient', 10, 2 );
function wc_new_order_billmelater_recipient( $recipient, $order ) {
if ( 'invoice' == $order->payment_method ) {
// $recipient .= ', ram@mobile-marketing.agency';
//$recipient .= ', emily@emilymorrowhome.com';
}
return $recipient;
}


// Making fields required / Custom Validation / Creating Custom Error Notices

add_action('woocommerce_checkout_process', 'pobox_required');

function pobox_required() {

// you can add any custom validations here

if ( $_POST['payment_method']=='invoice' && empty( $_POST['pobox'] ) )
wc_add_notice( 'Please enter your PO Box.', 'error' );

}





// Show coupon field on cart page for flooring-squares

function hide_coupon_field_on_cart( $enabled ) {
    global $woocommerce;

	foreach ( WC()->cart->get_cart() as  $cart_item_key => $cart_item ){
        if ( has_term( 'flooring-squares' , 'product_cat' , $cart_item['product_id'] ) ) { 
        $enabled = true;
        }
        else{
            $enabled = false;
        }
	}
	return $enabled;
}
//add_filter( 'woocommerce_coupons_enabled', 'hide_coupon_field_on_cart' );

/**
 * Hide product price based on user role and category.
 */
 function ace_hide_prices_guests( $price ) {
    global $product;
    global $role_emily;
    global $user_emily;
    global $array_unique;
    $user = wp_get_current_user();
    $role = ( array ) $user->roles;
    //print_r($role);

    //array_intersect( $role_emily, $user_emily->roles)
    
	if ( ! is_user_logged_in() && is_product_category( 'flooring' ) ) {
		return ''; // Return a empty string for no price display.
    }
    else if ( ! is_user_logged_in() && is_product_category( 'architectural-series-category' ) ) {
		return ''; // Return a empty string for no price display.
    }
    else if(is_user_logged_in() && $role[0]!='dealer-1' && is_product_category( 'flooring' ))
    { 
        return '';
    }
	return $price;
}
add_filter( 'woocommerce_get_price_html', 'ace_hide_prices_guests' ); // Hide product price


// Action to hide price and quantity, addtocart on single product 
// Single product pages
add_action( 'woocommerce_single_product_summary', 'hide_single_product_prices', 1 );
function hide_single_product_prices(){
    global $product;
    global $array_unique;
    $user = wp_get_current_user();
    $role = ( array ) $user->roles;
    global $role_emily;
    global $user_emily;

    if( (has_term( 'flooring', 'product_cat', $product->get_id() )) || (has_term( 'architectural-series-category', 'product_cat', $product->get_id() )) ){

        remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
        add_action( 'woocommerce_single_product_summary', 'custom_product_button', 30 );
    }
    
   /* if( has_term( 'flooring', 'product_cat', $product->get_id() ) && !is_user_logged_in() ){

    
    remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
    add_action( 'woocommerce_single_product_summary', 'custom_product_button', 30 );

    } else if(is_user_logged_in() && (count($array_unique)== 0) &&  has_term( 'flooring', 'product_cat', $product->get_id() )){
      
    remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
    add_action( 'woocommerce_single_product_summary', 'custom_product_button', 30 );

    }*/
}

// Action to hide fields
add_action('wp_head', 'add_css_head');
function add_css_head() {
    global $product;
    global $array_unique;
    $user = wp_get_current_user();
    $role = ( array ) $user->roles;

    if( has_term( array('flooring'), 'product_cat', $product->ID )){

   ?>
<style>
.single-product .productinfo-show-discounts{
    display:none;
}
 </style>

<?php
    }

    if( has_term( array('flooring', 'molding'), 'product_cat', $product->ID )){

        
?>

<style>

.single-product .calculated-price ,
.single-product .single_add_to_cart_button,
.single-product .quantity ,
.single-product .price,
.single-product #price_calculator,
.single-product .variations,
.single-product .wc-measurement-price-calculator-price{
    display:none;
}
 </style>

 <?php }
    if( has_term( array('flooring', 'molding'), 'product_cat', $product->ID ) && !is_user_logged_in() ){
?>
  <style>
.single-product #price_calculator,
.single-product .quantity,
.single-product .single_add_to_cart_button,
.single-product .productinfo-show-discounts{
    display:none;
}

 </style>
   
   <?php
    }
    else if((!is_user_logged_in() && has_term( array('flooring'), 'product_cat' )) || (is_user_logged_in() && (count($array_unique)== 0) && has_term( array('flooring', 'molding'), 'product_cat', $product->ID )) || (is_user_logged_in() && has_term( array('architectural-series-category'), 'product_cat' )) || (!is_user_logged_in() && has_term( array('architectural-series-category'), 'product_cat' )))
    
    {
        ?>
  <style>

.single-product #price_calculator,
.single-product .quantity,
.single-product .single_add_to_cart_button,
.single-product .productinfo-show-discounts{
    display:none;
}

 </style>
   
   <?php
         
    }
}

// Replacing the button add to cart by a link to the product in Shop and archives pages for as specific product category
// The custom replacement button function
function custom_product_button(){
    // HERE your custom button text and link
    $button_text = __( "Find Dealer", "woocommerce" );
    $button_link = ' /retailers/';
    global $product;
    if( has_term( 'architectural-series-category', 'product_cat', $product->get_id() ) )
        {
            echo '<div class="custom_dealerbtn"><img src="/wp-content/uploads/2019/08/ASLogo-VF-ltbgkd-1536x332.png" width="300"/></div><div class="custom_dealerbtn"><div class="textleft"><p>Find this flooring at a<br> retailer near you</p><a class="button" href="'.$button_link.'">' . $button_text . '</a></div><div class="textimgright"><img src="https://www.emilymorrowhome.com/wp-content/uploads/2019/11/OMGproof_logo-SHIELD-CREST-1.png" width="105"/></div></div>';

        }
        else
        {
            echo '<div class="custom_dealerbtn"><div class="textleft"><p>Find this flooring at a<br> retailer near you</p><a class="button" href="'.$button_link.'">' . $button_text . '</a></div><div class="textimgright"><img src="https://www.emilymorrowhome.com/wp-content/uploads/2019/11/OMGproof_logo-SHIELD-CREST-1.png" width="105"/> </div></div>';
        }
    // Display button
   
}

//Turn off Related Products in WooCommerce Product Pages

// $user_d= wp_get_current_user();
// $role_d = ( array ) $user_d->roles;
// global $product;

// //if((!is_user_logged_in() && has_term( 'flooring', 'product_cat', $product->ID )) || (is_user_logged_in() && $role_d[0]!='dealer-1' && has_term( 'flooring', 'product_cat', $product->ID ))){
//     if( has_term( 'flooring', 'product_cat', $product->ID )){
//         echo 'erer';
// remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 30 );
// }

add_action( 'wp', 'vn_remove_related_products' );
function vn_remove_related_products() {
$user_d= wp_get_current_user();
$role_d = ( array ) $user_d->roles;
global $array_unique;
 // if ( !is_user_logged_in() && has_term( array('flooring'), 'product_cat' ) ) {
if((!is_user_logged_in() && has_term( array('flooring','molding'), 'product_cat' )) || (is_user_logged_in() && (count($array_unique)== 0) && has_term( array('flooring','molding'), 'product_cat' )) || (!is_user_logged_in() && has_term( array('architectural-series-category'), 'product_cat' )) || (is_user_logged_in()  && has_term( array('architectural-series-category'), 'product_cat' ))){
        remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );
  }
}

/**
 * Redirects to another page.
 * 
 */
if( !function_exists('wc_custom_user_redirect') ) {
    function wc_custom_user_redirect( $redirect, $user ) {
        // Get the first of all the roles assigned to the user
        global $role_emily;
        global $user_emily;
        
        $role = $user->roles[0];
        $dashboard = admin_url();
       // $test= array_intersect( $role_emily, $user->roles);
       
       if ( $role == 'dealer-1' ) {        
        $redirect = home_url().'/lad-portal-home/';
        wp_redirect( $redirect );
        exit;
    } 
    else if ( $role == 'retailer-lad' ) 
    {
        $redirect = home_url().'/lad-portal-home/';
        wp_redirect( $redirect );
        exit;
    }
    else  
    {
        $redirect = home_url().'/';
        wp_redirect( $redirect );
        exit;
    }
       // return $redirect;
    }
}

add_filter( 'woocommerce_login_redirect', 'wc_custom_user_redirect', 10, 2 );


add_filter( 'wpsl_templates', 'custom_templates' );

function custom_templates( $templates ) {

    /**
     * The 'id' is for internal use and must be unique ( since 2.0 ).
     * The 'name' is used in the template dropdown on the settings page.
     * The 'path' points to the location of the custom template,
     * in this case the folder of your active theme.
     */
    $templates[] = array (
        'id'   => 'custom',
        'name' => 'Custom template',
        'path' => get_stylesheet_directory() . '/' . 'wpsl-templates/custom.php',
    );

    return $templates;
}


add_filter( 'wpsl_listing_template', 'custom_listing_template' );

function custom_listing_template() {

    global $wpsl, $wpsl_settings;
    
    $listing_template = '<li data-store-id="<%= id %>">' . "\r\n";
    $listing_template .= "\t\t" . '<div class="emily-store">' . "\r\n";
    $listing_template .= "\t\t\t" . '<p><%= thumb %>' . "\r\n";
    $listing_template .= "\t\t\t\t" . wpsl_store_header_template( 'listing' ) . "\r\n";
    $listing_template .= "\t\t\t\t" . '<span class="wpsl-street"><%= address %></span>' . "\r\n";
    $listing_template .= "\t\t\t\t" . '<% if ( address2 ) { %>' . "\r\n";
    $listing_template .= "\t\t\t\t" . '<span class="wpsl-street"><%= address2 %></span>' . "\r\n";
    $listing_template .= "\t\t\t\t" . '<% } %>' . "\r\n";
    $listing_template .= "\t\t\t\t" . '<span>' . wpsl_address_format_placeholders() . '</span>' . "\r\n";
    $listing_template .= "\t\t\t\t" . '<span class="wpsl-country"><%= country %></span>' . "\r\n";
    $listing_template .= "\t\t\t" . '</p>' . "\r\n";
    
    // Show the phone, fax or email data if they exist.
    $listing_template .= "\t\t\t" . '<p class="wpsl-contact-details">' . "\r\n";
    $listing_template .= "\t\t\t" . '<% if ( phone ) { %>' . "\r\n";
    $listing_template .= "\t\t\t" . '<span><i class="fa fa-phone" aria-hidden="true"></i><%= formatPhoneNumber( phone ) %></span>' . "\r\n";
    $listing_template .= "\t\t\t" . '<% } %>' . "\r\n";
    $listing_template .= "\t\t\t" . '<% if ( fax ) { %>' . "\r\n";
    $listing_template .= "\t\t\t" . '<span><strong>' . esc_html( $wpsl->i18n->get_translation( 'fax_label', __( 'Fax', 'wpsl' ) ) ) . '</strong>: <%= fax %></span>' . "\r\n";
    $listing_template .= "\t\t\t" . '<% } %>' . "\r\n";
    $listing_template .= "\t\t\t" . '<% if ( email ) { %>' . "\r\n";
    $listing_template .= "\t\t\t" . '<span><strong>' . esc_html( $wpsl->i18n->get_translation( 'email_label', __( 'Email', 'wpsl' ) ) ) . '</strong>: <%= email %></span>' . "\r\n";
    $listing_template .= "\t\t\t" . '<% } %>' . "\r\n";
    $listing_template .= "\t\t\t" . '<% if ( url ) { %>' . "\r\n";
        $listing_template .= "\t\t\t" . '<span><i class="fa fa-globe" aria-hidden="true"></i><a href="<%= url %>" target="blank"><%= url %></a></span>' . "\r\n";
        $listing_template .= "\t\t\t" . '<% } %>' . "\r\n";
    $listing_template .= "\t\t\t" . '</p>' . "\r\n";
    
    $listing_template .= "\t\t" . '</div>' . "\r\n";
 
    $listing_template .= "\t\t" . '<%= createDirectionUrl() %>' . "\r\n"; 
    $listing_template .= "\t" . '</li>' . "\r\n";

    return $listing_template;
}


// change/add fistance in header of store
add_filter( 'wpsl_store_header_template', 'custom_store_header_template' );

function custom_store_header_template() {
    
    $header_template = '<div class="emily-header-store"><% if ( wpslSettings.storeUrl == 1 && url ) { %>' . "\r\n";
    $header_template .= '<h3><a href="<%= url %>"><%= store %></a></h3>' . "\r\n";
    $header_template .= '<% } else { %>' . "\r\n";
    $header_template .= '<h3><%= store %></h3>' . "\r\n";   
    
    // Check if we need to show the distance.
    if ( !$wpsl_settings['hide_distance'] ) {
        $header_template .= "\t\t" . '<span class="emily-distance"><%= distance %> mi' . esc_html( $wpsl_settings['distance_unit'] ) . '</span>' . "\r\n";
    }   
    $header_template .= '<% } %></div>'; 
    
    return $header_template;
}

//hide start location marker
add_filter( 'wpsl_js_settings', 'custom_js_settings' );

function custom_js_settings( $settings ) {

    $settings['startMarker'] = '';

    return $settings;
}


function cptui_register_my_cpts_lad_producttraining() {

	/**
	 * Post Type: LAD Product Training.
	 */

	$labels = array(
		"name" => __( "LAD Product Training", "emh" ),
		"singular_name" => __( "LAD Product Training", "emh" ),
	);

	$args = array(
		"label" => __( "LAD Product Training", "emh" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"delete_with_user" => false,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "lad_producttraining", "with_front" => true ),
		"query_var" => true,
		"supports" => array( "title", "editor", "thumbnail" ),
	);

	register_post_type( "lad_producttraining", $args );
}

add_action( 'init', 'cptui_register_my_cpts_lad_producttraining' );

function cptui_register_my_cpts_lad_market_material() {

	/**
	 * Post Type: LAD Marketing Materials.
	 */

	$labels = array(
		"name" => __( "LAD Marketing Materials", "emh" ),
		"singular_name" => __( "LAD Marketing Material", "emh" ),
	);

	$args = array(
		"label" => __( "LAD Marketing Materials", "emh" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"delete_with_user" => false,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "lad_market_material", "with_front" => true ),
		"query_var" => true,
		"supports" => array( "title", "thumbnail" ),
	);

	register_post_type( "lad_market_material", $args );
}

add_action( 'init', 'cptui_register_my_cpts_lad_market_material' );

function cptui_register_my_taxes_lad_marketing_cat() {

	/**
	 * Taxonomy: LAD Materials Categories.
	 */

	$labels = array(
		"name" => __( "LAD Materials Categories", "emh" ),
		"singular_name" => __( "LAD Materials Category", "emh" ),
	);

	$args = array(
		"label" => __( "LAD Materials Categories", "emh" ),
		"labels" => $labels,
		"public" => true,
		"publicly_queryable" => true,
		"hierarchical" => true,
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'lad_marketing_cat', 'with_front' => true, ),
		"show_admin_column" => false,
		"show_in_rest" => true,
		"rest_base" => "lad_marketing_cat",
		"rest_controller_class" => "WP_REST_Terms_Controller",
		"show_in_quick_edit" => false,
		);
	register_taxonomy( "lad_marketing_cat", array( "lad_market_material" ), $args );
}
add_action( 'init', 'cptui_register_my_taxes_lad_marketing_cat' );

function cptui_register_my_taxes_lad_training_cat() {

	/**
	 * Taxonomy: LAD Training Categories.
	 */

	$labels = array(
		"name" => __( "LAD Training Categories", "emh" ),
		"singular_name" => __( "LAD Training Category", "emh" ),
	);

	$args = array(
		"label" => __( "LAD Training Categories", "emh" ),
		"labels" => $labels,
		"public" => true,
		"publicly_queryable" => true,
		"hierarchical" => true,
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'lad_training_cat', 'with_front' => true, ),
		"show_admin_column" => false,
		"show_in_rest" => true,
		"rest_base" => "lad_training_cat",
		"rest_controller_class" => "WP_REST_Terms_Controller",
		"show_in_quick_edit" => false,
		);
	register_taxonomy( "lad_training_cat", array( "lad_producttraining" ), $args );
}
add_action( 'init', 'cptui_register_my_taxes_lad_training_cat' );
// Added to extend allowed files types in Media upload
add_filter('upload_mimes', 'custom_upload_mimes');
function custom_upload_mimes ( $existing_mimes=array() ) {

// Add *.EPS files to Media upload
$existing_mimes['eps'] = 'application/postscript';

// Add *.AI files to Media upload
$existing_mimes['ai'] = 'application/postscript';
// Add *.idml files to Media upload
$existing_mimes['idml'] = 'application/postscript';

return $existing_mimes;
} 

function so174837_registration_email_alert( $user_id ) {
    $user    = get_userdata( $user_id );
    $email   = $user->user_email;
    $message .= $email . ' has registered to your website.';
    wp_mail( 'Help@emilymorrowhome.com', 'Emily Morrow Home - New User registration', $message );
    wp_mail( 'Info@emilymorrowhome.com', 'Emily Morrow Home - New User registration', $message );
}
add_action('user_register', 'so174837_registration_email_alert');