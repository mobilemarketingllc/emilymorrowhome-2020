jQuery(document)
    .ready(function(a) {
        function b(b) {
            var c = null;
            return a.each(wc_price_calculator_params.pricing_rules, function(a, d) {
                if (b >= parseFloat(d.range_start) && ("" === d.range_end || b <= d.range_end)) return c = d, !1
            }), c
        }

        function c(b) {
            void 0 === wc_price_calculator_params.page_loaded ? (d(b), wc_price_calculator_params.page_loaded = !0) : setTimeout(function() {
                var c = {};
                b.find(".amount_needed:input, input[name=quantity]")
                    .each(function(a, b) {
                        c[b.name] = b.value
                    }), a.cookie(wc_price_calculator_params.cookie_name, c)
            }, 100)
        }

        function d(b) {
            var c = a.cookie(wc_price_calculator_params.cookie_name);
            if (!1 !== a.isPlainObject(c) && !a.isEmptyObject(c)) {
                for (var d in c) !1 !== c.hasOwnProperty(d) && b.find('.amount_needed[name="' + d + '"]:not(.fixed-value), input[name="' + d + '"].qty')
                    .val(c[d]);
                setTimeout(function(a) {
                    return function() {
                        a.trigger("mpc-change")
                    }
                }(b.find("input.amount_needed:first")), 100)
            }
        }

        function e(a, b, c, d) {
            a = (a + "")
                .replace(/[^0-9+\-Ee.]/g, "");
            var e = isFinite(+a) ? +a : 0,
                f = isFinite(+b) ? Math.abs(b) : 0,
                g = "undefined" == typeof d ? "," : d,
                h = "undefined" == typeof c ? "." : c,
                i = "",
                j = function(a, b) {
                    var c = Math.pow(10, b);
                    return "" + Math.round(a * c) / c
                };
            return i = (f ? j(e, f) : "" + Math.round(e))
                .split("."), i[0].length > 3 && (i[0] = i[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, g)), (i[1] || "")
                .length < f && (i[1] = i[1] || "", i[1] += new Array(f - i[1].length + 1)
                    .join("0")), i.join(h)
        }

        function f(a, b) {
            return (a + "")
                .replace(new RegExp("[.\\\\+*?\\[\\^\\]$(){}=!<>|:\\" + (b || "") + "-]", "g"), "\\$&")
        }

        function g(a, b, c) {
            return "undefined" != typeof wc_price_calculator_params.unit_normalize_table[b] && ("undefined" != typeof wc_price_calculator_params.unit_normalize_table[b].inverse && wc_price_calculator_params.unit_normalize_table[b].inverse ? a /= wc_price_calculator_params.unit_normalize_table[b].factor : a *= wc_price_calculator_params.unit_normalize_table[b].factor, b = wc_price_calculator_params.unit_normalize_table[b].unit), "undefined" != typeof wc_price_calculator_params.unit_conversion_table[b] && "undefined" != typeof wc_price_calculator_params.unit_conversion_table[b][c] && ("undefined" != typeof wc_price_calculator_params.unit_conversion_table[b][c].inverse && wc_price_calculator_params.unit_conversion_table[b][c].inverse ? a /= wc_price_calculator_params.unit_conversion_table[b][c].factor : a *= wc_price_calculator_params.unit_conversion_table[b][c].factor), a
        }

        function h(a) {
            var b;
            return (b = a.match(/(\d+)\s+(\d+)\/(\d+)/)) ? 0 !== b[3] ? parseFloat(b[1]) + b[2] / b[3] : parseFloat(b[1]) : (b = a.match(/(\d+)\/(\d+)/)) ? 0 !== b[2] ? b[1] / b[2] : 0 : "" === a ? 0 : parseFloat(a)
        }

        function i(a) {
            var b = "",
                c = wc_price_calculator_params.woocommerce_price_num_decimals,
                d = wc_price_calculator_params.woocommerce_currency_pos,
                f = wc_price_calculator_params.woocommerce_currency_symbol;
            switch (a = e(a, c, wc_price_calculator_params.woocommerce_price_decimal_sep, wc_price_calculator_params.woocommerce_price_thousand_sep), "yes" === wc_price_calculator_params.woocommerce_price_trim_zeros && c > 0 && (a = j(a)), d) {
                case "left":
                    b = '<span class="amount">' + f + a + "</span>";
                    break;
                case "right":
                    b = '<span class="amount">' + a + f + "</span>";
                    break;
                case "left_space":
                    b = '<span class="amount">' + f + "&nbsp;" + a + "</span>";
                    break;
                case "right_space":
                    b = '<span class="amount">' + a + "&nbsp;" + f + "</span>"
            }
            return b
        }

        function j(a) {
            return a.replace(new RegExp(f(wc_price_calculator_params.woocommerce_price_decimal_sep, "/") + "0+$"), "")
        }
        a.cookie.json = !0, a(document)
            .bind("reset_image", function() {
                wc_price_calculator_params.product_price = "", wc_price_calculator_params.product_measurement_value = "", wc_price_calculator_params.product_measurement_unit = "", a(".variable_price_calculator")
                    .hide()
            }), "undefined" != typeof wc_price_calculator_params && "pricing" === wc_price_calculator_params.calculator_type && (a("form.cart")
                .bind("wc-measurement-price-calculator-update", function() {
                    var c;
                    if (a(".amount_needed:input")
                        .each(function(b, c) {
                            c = a(c);
                            var d = c.val()
                                .replace(wc_price_calculator_params.woocommerce_price_decimal_sep, "."),
                                e = h(d);
                            c.trigger("wc-measurement-price-calculator-product-measurement-change", [e])
                        }), a(".amount_needed:input")
                        .each(function(b, d) {
                            d = a(d);
                            var e = d.val()
                                .replace(wc_price_calculator_params.woocommerce_price_decimal_sep, "."),
                                f = h(e);
                            if (!f || f < 0) return c = 0, !1;
                            if (f = g(f, d.data("unit"), d.data("common-unit")), "area-linear" === wc_price_calculator_params.measurement_type) c ? c += 2 * f : c = 2 * f;
                            else if ("area-surface" === wc_price_calculator_params.measurement_type) {
                                if (!c) {
                                    var i = a("#length_needed")
                                        .val()
                                        .replace(wc_price_calculator_params.woocommerce_price_decimal_sep, ".");
                                    i = g(h(i), a("#length_needed")
                                        .data("unit"), a("#length_needed")
                                        .data("common-unit"));
                                    var j = a("#width_needed")
                                        .val()
                                        .replace(wc_price_calculator_params.woocommerce_price_decimal_sep, ".");
                                    j = g(h(j), a("#width_needed")
                                        .data("unit"), a("#width_needed")
                                        .data("common-unit"));
                                    var k = a("#height_needed")
                                        .val()
                                        .replace(wc_price_calculator_params.woocommerce_price_decimal_sep, ".");
                                    return k = g(h(k), a("#height_needed")
                                        .data("unit"), a("#height_needed")
                                        .data("common-unit")), void(c = 2 * (i * j + j * k + i * k))
                                }
                            } else c ? c *= f : c = f
                        }), c = g(c, wc_price_calculator_params.product_total_measurement_common_unit, wc_price_calculator_params.product_price_unit), c = parseFloat(c.toFixed(wc_price_calculator_params.measurement_precision)), wc_price_calculator_params.pricing_rules) {
                        var d = b(c);
                        d ? (wc_price_calculator_params.product_price = parseFloat(d.price), a(".single_variation span.price")
                            .html(d.price_html)) : (wc_price_calculator_params.product_price = "", a(".single_variation span.price")
                            .html(""))
                    }
                    a("#_measurement_needed")
                        .val(c), a("#_measurement_needed_unit")
                        .val(wc_price_calculator_params.product_price_unit);
                    var e = 0,
                        f = 0,
                        j = parseFloat(wc_price_calculator_params.pricing_overage),
                        k = a(".product_price"),
                        l = a(".product_price_overage");
                    if (c ? (e = wc_price_calculator_params.product_price * c, wc_price_calculator_params.minimum_price > e && (e = parseFloat(wc_price_calculator_params.minimum_price)), j > 0 && (f = e * j, e += f, l.html(i(f))), k.html(i(e))
                            .trigger("wc-measurement-price-calculator-product-price-change", [c, e])) : (k.html("")
                            .trigger("wc-measurement-price-calculator-product-price-change"), j > 0 && l.html("")), a(".wc-measurement-price-calculator-total-amount")) {
                        var m = g(c, wc_price_calculator_params.product_price_unit, a(".wc-measurement-price-calculator-total-amount")
                            .data("unit"));
                        m = parseFloat(c.toFixed(wc_price_calculator_params.measurement_precision)), a(".wc-measurement-price-calculator-total-amount")
                            .text(m)
                    }
                    if ("undefined" != typeof woocommerce_addons_params && a("form.cart")
                        .find("#product-addons-total")
                        .length > 0) {
                        var n = "" === e ? 0 : e;
                        woocommerce_addons_params.product_price = n.toFixed(2), a("form.cart")
                            .trigger("woocommerce-product-addons-update")
                    }
                }), a("form.cart")
                .trigger("wc-measurement-price-calculator-update"), a(".amount_needed:input")
                .on("keyup change mpc-change", function() {
                    var b = a(this)
                        .closest("form.cart");
                    b.trigger("wc-measurement-price-calculator-update"), c(b)
                })
                .first()
                .trigger("mpc-change"), a(".single_variation, .single_variation_wrap")
                .bind("show_variation", function(b, c) {
                    var d = parseFloat(c.price);
                    wc_price_calculator_params.product_price = d, a("form.cart")
                        .trigger("wc-measurement-price-calculator-show-variation", c), a("form.cart")
                        .trigger("wc-measurement-price-calculator-update"), a(".variable_price_calculator")
                        .show()
                }), a(document.body)
                .bind("updated_addons", function() {
                    var b = a("form.cart"),
                        c = b.find("#product-addons-total"),
                        d = c.data("price");
                    d !== woocommerce_addons_params.product_price && c.length > 0 && (c.data("price", woocommerce_addons_params.product_price), b.trigger("woocommerce-product-addons-update"))
                })), "undefined" != typeof wc_price_calculator_params && "quantity" === wc_price_calculator_params.calculator_type && (a("form.cart")
                .bind("wc-measurement-price-calculator-quantity-changed", function(b, c) {
                    wc_price_calculator_params.product_measurement_value && (a(".amount_needed, .amount_actual")
                        .each(function(b, d) {
                            if (d = a(d), d.hasClass("amount_needed") && a(".amount_needed")
                                .length > 1) return !0;
                            var e = g(wc_price_calculator_params.product_measurement_value, wc_price_calculator_params.product_measurement_unit, d.data("unit"));
                            e = parseFloat((e * c)
                                .toFixed(2)), d.is("input") ? d.val(e) : d.text(e)
                        }), a(".total_price")
                        .html(i(c * wc_price_calculator_params.product_price))
                        .trigger("wc-measurement-price-calculator-quantity-total-price-change", [c, wc_price_calculator_params.product_price]))
                }), a("form.cart")
                .bind("wc-measurement-price-calculator-update", function() {
                    if (wc_price_calculator_params.product_measurement_value) {
                        var b;
                        if (a("input.amount_needed")
                            .each(function(c, d) {
                                d = a(d);
                                var e = d.val()
                                    .replace(wc_price_calculator_params.woocommerce_price_decimal_sep, "."),
                                    f = h(e);
                                return !f || f < 0 ? (b = 0, !1) : (f = g(f, d.data("unit"), d.data("common-unit")), void(b ? b *= f : b = f))
                            }), b) {
                            var c = g(wc_price_calculator_params.product_measurement_value, wc_price_calculator_params.product_measurement_unit, wc_price_calculator_params.product_total_measurement_common_unit),
                                d = Math.ceil((b / c)
                                    .toFixed(wc_price_calculator_params.measurement_precision));
                            d < parseFloat(wc_price_calculator_params.quantity_range_min_value) && (d = parseFloat(wc_price_calculator_params.quantity_range_min_value)), parseFloat(wc_price_calculator_params.quantity_range_max_value) && d > parseFloat(wc_price_calculator_params.quantity_range_max_value) && (d = parseFloat(wc_price_calculator_params.quantity_range_max_value)), a("input[name=quantity]")
                                .val(d), a(".amount_actual")
                                .each(function(b, c) {
                                    c = a(c);
                                    var e = g(wc_price_calculator_params.product_measurement_value, wc_price_calculator_params.product_measurement_unit, c.data("unit"));
                                    e = parseFloat((e * d)
                                        .toFixed(2)), c.is("input") ? c.val(e) : c.text(e)
                                }), a(".total_price")
                                .html(i(d * wc_price_calculator_params.product_price))
                                .trigger("wc-measurement-price-calculator-total-price-change", [d, wc_price_calculator_params.product_price])
                        }
                    }
                }), a(".amount_needed:input")
                .on("keyup change mpc-change", function() {
                    var b = a(this)
                        .closest("form.cart");
                    b.trigger("wc-measurement-price-calculator-update"), c(b)
                }), a("input[name=quantity]")
                .on("keyup change mpc-change", function(b) {
                    var d = a(this)
                        .closest("form.cart");
                    d.trigger("wc-measurement-price-calculator-quantity-changed", [b.target.value]), c(d)
                })
                .trigger("change"), a(".single_variation, .single_variation_wrap")
                .bind("show_variation", function(b, c) {
                    wc_price_calculator_params.product_price = parseFloat(c.price), wc_price_calculator_params.product_measurement_value = parseFloat(c.product_measurement_value), wc_price_calculator_params.product_measurement_unit = c.product_measurement_unit, c.product_measurement_value ? (a("input.amount_needed")
                            .length > 0 && a("input.amount_needed")
                            .val() ? a("form.cart")
                            .trigger("wc-measurement-price-calculator-update") : a("form.cart")
                            .trigger("wc-measurement-price-calculator-quantity-changed", [a("input[name=quantity]")
                                .val()
                            ]), a(".variable_price_calculator")
                            .show()) : a(".variable_price_calculator")
                        .hide()
                }))
    });