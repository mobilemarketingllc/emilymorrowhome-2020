<?php
/**
 * The template for displaying the homepage.
 *
 * Template name: Order
 *
 * @package storefront
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main home-site-main" role="main">
		<div class="columns-3">
			<ul class="products">
		<?php 
$loop = new WP_Query( array( 'post_type' => 'ordersample') );
if ( $loop->have_posts() ) :
	while ( $loop->have_posts() ) : $loop->the_post(); ?>

<li class="post-73 product type-product status-publish has-post-thumbnail product_cat-flooring lifestyles-rugged-industrial first instock taxable shipping-taxable purchasable product-type-simple">
<a href="<?php the_permalink(); ?>" 
class="woocommerce-LoopProduct-link woocommerce-loop-product__link"><span class="woocommerce-loop-product-image-container">
<?php the_post_thumbnail(array(300,300)); ?></span><h3><?php echo get_the_title(); ?></h3>

</a></li>
<?php endwhile;endif;
		?>
			</ul>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->
	
        
<?php
get_footer();