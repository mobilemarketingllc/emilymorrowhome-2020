<?php
/**
 * The loop template file to display collections content.
 *
 * Included on pages like index.php, archive.php and search.php to display a loop of posts
 * Learn more: http://codex.wordpress.org/The_Loop
 *
 * @package storefront
 */

$categories = array(
	'Flooring',
	'Furniture',
	'Home Décor'
);

foreach ($categories as $category) {
	do_action( 'storefront_loop_before' );

	// Pull in flooring, furniture, and lighting
	echo '<h3>' . $category . '</h3>';
	$args = array(
		'post_type' => 'product',
		'tax_query' => array(
			array(
				'taxonomy' => 'collections',
				'field' => 'term_id',
				'terms' => get_queried_object_id()
			),
			array(
				'taxonomy' => 'product_cat',
				'field' => 'name',
				'terms' => $category
			)
		)
	);
	$query = new WP_Query($args);
	if ($query->have_posts()) {
		woocommerce_product_loop_start();
		while ($query->have_posts()) {
			$query->the_post();
			do_action('woocommerce_shop_loop');
			wc_get_template_part('content', 'product');
		}
		woocommerce_product_loop_end();
	}

	/**
	* Functions hooked in to storefront_paging_nav action
	*
	* @hooked storefront_paging_nav - 10
	*/
	do_action( 'storefront_loop_after' );
}
