<?php
/**
 * The template for a miscellaneous inner page.
 *
 * Template name: Misc. Inner Page 
 *
 * @package storefront
 */
function inner_body_class($classes) {
	$classes[] = 'misc-inner';
	return $classes;
}
add_filter('body_class', 'inner_body_class');

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main inner-site-main" role="main">
			<?php while (have_posts()) : the_post();
				do_action('storefront_page_before');
				get_template_part('content', 'page');
			endwhile; ?>
		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();