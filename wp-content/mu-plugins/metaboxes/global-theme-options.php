<?php

class theme_Admin {
    private $key = "theme_options";
    private $metabox_id = "theme_option_metabox";
    private $prefix = "emh_";
    protected $title = "";
    protected $options_page = "";
    
    public function __construct() {
        $this->title = __('Global Options', 'storefront');
    }

    public function hooks() {
        add_action('admin_init', array($this, 'init'));
        add_action('admin_menu', array($this, 'add_options_page'));
        add_action('cmb2_init', array($this, 'add_options_page_metabox'));
    }

    public function init() {
        register_setting($this->key, $this->key);
    }

    public function adminCSS() {

    }

    public function add_options_page() {
        $this->options_page = add_menu_page($this->title, $this->title, 'manage_options', $this->key, array($this, 'admin_page_display'));
        add_action("admin_print_styles-{$this->options_page}", array('CMB2_hookup', 'enqueue_cmb_css'));
        add_action("admin_print_styles-{$this->options_page}", array($this, 'adminCSS'));
    }
    
    public function admin_page_display() { ?>
        <div class="wrap cmb2-options-page <?php echo $this->key; ?>">
            <h2><?php echo esc_html(get_admin_page_title()); ?></h2>
            <?php cmb2_metabox_form($this->metabox_id, $this->key, array('cmb_styles' => false)); ?>
        </div>
    <?php }

    function add_options_page_metabox() {
        $cmb = new_cmb2_box(array(
            'id' => $this->metabox_id,
            'hookup' => false,
            'show_on' => array(
                // These are important, don't remove
                'key' => 'options-page',
                'value' => array($this->key)
            )
        ));

        // Social links
        $cmb->add_field(array(
            'id' => $this->prefix . 'facebook_url',
            'name' => __('Facebook URL', 'storefront'),
            'type' => 'text_url'
        ));
        $cmb->add_field(array(
            'id' => $this->prefix . 'twitter_url',
            'name' => __('Twitter URL', 'storefront'),
            'type' => 'text_url'
        ));
        $cmb->add_field(array(
            'id' => $this->prefix . 'pinterest_url',
            'name' => __('Pinterest URL', 'storefront'),
            'type' => 'text_url'
        ));
        $cmb->add_field(array(
            'id' => $this->prefix . 'instagram_url',
            'name' => __('Instagram URL', 'storefront'),
            'type' => 'text_url'
        ));
        $cmb->add_field(array(
            'id' => $this->prefix . 'linkedin_url',
            'name' => __('LinkedIn URL', 'storefront'),
            'type' => 'text_url'
        ));
        $cmb->add_field(array(
            'id' => $this->prefix . 'googleplus_url',
            'name' => __('Google+ URL', 'storefront'),
            'type' => 'text_url'
        ));
    }

    public function __get($field) {
		// Allowed fields to retrieve
		if (in_array($field, array('key', 'metabox_id', 'title', 'options_page'), true)) {
			return $this->{$field};
		}
		throw new Exception('Invalid property: ' . $field);
	}
}

function theme_admin() {
    static $object = null;
    if (is_null($object)) {
        $object = new theme_Admin();
        $object->hooks();
    }
    return $object;
}

function theme_get_option($key = '') {
    return cmb2_get_option(theme_admin()->key, $key);
}

// Get it started
theme_admin();